export default {

  today: new Date(),
  moisLettre: ["", "janv" , "fév", "mars", "avril", "mai", "juin", "juillet", "aout", "sept", "oct", "nov", "déc"],
  dayLettre: ["dimanche", "lundi", "mardi", "mercredi","jeudi", "vendredi", "samedi"],

  currentMonth() {
    return this.today.getMonth() +1;
 },

  leMois(current) {
    return this.moisLettre[current];
  },

  leJour() {
    return this.dayLettre[this.today.getDay()];
 },

  laDateJour() {
    return this.today.getDate();
 },

  Lannee() {
    return this.today.getFullYear();
 },
 //ATTENTION  month -1 sur certain pour résoudre le probleme de JS
 //day.getMonth()  ->   [0,11]
 nombreJours(month, year) {
   const rep = [];
   const x = new Date(year, month, 0).getDate();
   let premierJourDuMois = new Date(year, month -1, 1).getDay();
   premierJourDuMois = (premierJourDuMois) === 0 ? 7 : premierJourDuMois;
   // != lundi, on rajoute des cases vides au début
   if (premierJourDuMois != 1 ) {
     for (let i=1; i< premierJourDuMois; i++){
       rep.push({});
     }
   } // ajout des jours dans un array.
   for ( let i=1; i<= x; i++) {
     
     let jour = {};
     jour.number = i;
     jour.day = new Date(year, month -1, i).getDay();
     jour.month = month;
     jour.monthLettre = this.leMois(month);
     jour.year = year;
     rep.push(jour);
   }
   return rep;
 }
}; // fin this

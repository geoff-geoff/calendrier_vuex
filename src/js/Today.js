import lesDates from '../js/lesDates.js';


export default {
  currentMonth: lesDates.currentMonth(), // en chiffre
  year: lesDates.Lannee(), // en chiffre
  mois: lesDates.leMois(lesDates.currentMonth()), // en lettre
  today: lesDates.leJour(), //en lettre
  dateJour: lesDates.laDateJour(), //en chiffre  bour le bouton revenir à aujourd'hui
};

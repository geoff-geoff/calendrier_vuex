import Vue from 'vue';
import Vuex from 'vuex';


import lesDates from './js/lesDates.js';

Vue.use(Vuex);



const state = {
  today: {
    month: lesDates.currentMonth(), // en chiffre
    year: lesDates.Lannee(), // en chiffre
    mois: lesDates.leMois(lesDates.currentMonth()), // en lettre
    today: lesDates.leJour(), //en lettre
    number: lesDates.laDateJour(), //en chiffre  bour le bouton revenir à aujourd'hui
  },
  ceMois: lesDates.currentMonth(),
  ceAnnee: lesDates.Lannee(),
  nombresJours: lesDates.nombreJours(lesDates.currentMonth(), lesDates.Lannee()), //pour les cases
  specificday: {
    month: lesDates.currentMonth(), // en chiffre
    year: lesDates.Lannee(), // en chiffre
    monthLettre: lesDates.leMois(lesDates.currentMonth()), // en lettre
    today: lesDates.leJour(), //en lettre
    number: lesDates.laDateJour(), //en chiffre  bour le bouton revenir à aujourd'hui
  },
  events : [{number: 1,
               month: 11,
               titre: "test",
               text: "Jean Claude is content ;)",
               heuredebut: "10:12",
               heurefin: "12:00"},
             {number: 1,
                month: 10,
                text: "Et toi tu en penses quoi, Are you Happy"},
             {number: 1,
                month: 9,
                text: "jean  ;)"},
              {number: 1,
                month: 10,
                text: "jean  ;)"}], //end events
};


//juste pour rigoler, et montrer que ça fonctionne comme ça aussi   HA HA HAHAHAHA HAHahahaahaaaaa   ah   ahah
const remiseAzero =  (a,b) => state.nombresJours = lesDates.nombreJours(a,b);




export default new Vuex.Store({
  state: state,

  getters: {
    getSpecific: (state) =>  { return {number: state.specificday.number, month: state.specificday.month};},
    getSpecificDay: (state) => {return state.specificday;},
    getEvents: (state) => { return state.events;}
  },

  mutations: {
    MORE_MONTH(state){
      state.ceMois += 1;
      if (state.ceMois > 12) {
        state.ceMois = 1;
        state.ceAnnee += 1;
      }
       remiseAzero(state.ceMois, state.ceAnnee);
    },
    LESS_MONTH(state){
      state.ceMois -= 1;
      if (state.ceMois < 1) {
        state.ceMois = 12;
        state.ceAnnee -= 1;
      }
       remiseAzero(state.ceMois, state.ceAnnee);
    },
    BACK_TODAY(state){
      state.ceMois = state.today.month;
      state.ceAnnee = state.today.year;
      state.specificday = state.today;
      remiseAzero(state.ceMois, state.ceAnnee);
    },
    CHANGE_SPECIFICDAY(state, d){
      state.specificday = d;
    },

    // events
    DEL_EVENT(state,id){
      state.events.splice(id, 1);
    },
    ADD_EVENT(state,ev){
      state.events.push(ev);
    }
  },
  actions: {

  }
});
